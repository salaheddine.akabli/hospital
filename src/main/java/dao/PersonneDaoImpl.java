package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Personne;

public class PersonneDaoImpl implements Dao<Personne>{
	
	@Override
	public Personne findById(int id) {
		Connection connection;
		try {
			connection = DbConnection.getConnection();
			String sql = "SELECT * FROM personne WHERE personne.id = ?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new Personne(
						rs.getInt("id"),
						rs.getString("nom"),
						rs.getString("prenom"),
						rs.getString("sexe"),
						rs.getString("email"),
						rs.getString("password"),
						rs.getString("telephone"),
						rs.getDate("dateNaissance")
						);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ArrayList<Personne> findAll() {
		Connection connection;
		ArrayList<Personne> personnes = new ArrayList<Personne>();
		try {
			connection = DbConnection.getConnection();
			String sql = "SELECT * FROM personne;";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				personnes.add(
						new Personne(
								rs.getInt("id"),
								rs.getString("nom"),
								rs.getString("prenom"),
								rs.getString("sexe"),
								rs.getString("email"),
								rs.getString("password"),
								rs.getString("telephone"),
								rs.getDate("dateNaissance")
								)
						);
			}
			return personnes;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void save(Personne p) {
		Connection connection;
		try {
			connection = DbConnection.getConnection();
			String sql = "INSERT INTO personne(`nom`, `prenom`, `sexe`, `email`, `password`, `telephone`, `dateNaissance`)"
					+ "values(?, ?, ?, ?, ?, ?, ?);";
			PreparedStatement ps = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, p.getNom());
			ps.setString(2, p.getPrenom());
			ps.setString(3, p.getSexe());
			ps.setString(4, p.getEmail());
			ps.setString(5, p.getPassword());
			ps.setString(6, p.getTelephone());
			ps.setDate(7, p.getDateNaissance());
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(int id, Personne p) {
		Connection connection;
		try {
			connection = DbConnection.getConnection();
			String sql = "UPDATE personne set name = ?, prenom = ?, sexe=?, email=?, password=?, telephone=?, dateNaissance=? where id=?;";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, p.getNom());
			ps.setString(2, p.getPrenom());
			ps.setString(3, p.getSexe());
			ps.setString(4, p.getEmail());
			ps.setString(5, p.getPassword());
			ps.setString(6, p.getTelephone());
			ps.setDate(7, p.getDateNaissance());
			ps.setInt(8, id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int id) {
		try {
			Connection connection = DbConnection.getConnection();
			String sql = "DELETE FROM personne where personne.id = ?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Personne getByEmailAndPassword(String email, String password) {
		try {
			Connection connection = DbConnection.getConnection();
			String sql = "SELECT * FROM personne WHERE personne.email = ? and personne.password = ?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new Personne(
						rs.getInt("id"),
						rs.getString("nom"),
						rs.getString("prenom"),
						rs.getString("sexe"),
						rs.getString("email"),
						rs.getString("password"),
						rs.getString("telephone"),
						rs.getDate("dateNaissance")
						);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
