package dao;

import java.util.ArrayList;

import model.Personne;

public interface Dao<Type> {
	
	Type findById(int id);
	ArrayList<Type> findAll();
	void save(Type t);
	void update(int id,Type t);
	void delete(int id);
	
	Personne getByEmailAndPassword(String email,String password);
}
