package model;

import java.sql.Date;

import enums.Role;

public class Major extends Personne{
	
//	final private Role role = Role.MAJOR;
	
	private String password;
	
	public Major() {
		super();
	}
	
	public Major(int id, String nom, String prenom, String sexe, String email, String password, String telephone,
			Date dateNaissance,String specialite) {
		super(id, nom, prenom, sexe, email, password, telephone, dateNaissance);
		
	}
	
	public Major(String nom, String prenom, String sexe, String email, String password, String telephone,
			Date dateNaissance,String specialite) {
		super(nom, prenom, sexe, email, telephone, dateNaissance);
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
/*
	public Role getRole() {
		return role;
	}
*/	
}
