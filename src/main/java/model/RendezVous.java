package model;

import java.sql.Date;

public class RendezVous {
	
	private int id;
	private int idMedecin;
	private int idPatient;
	private int idRapport;
	private Date dateRendezVous;
	
	public RendezVous() {
	}

	public RendezVous(int id, int idMedecin, int idPatient, int idRapport, Date dateRendezVous) {
		this.id = id;
		this.idMedecin = idMedecin;
		this.idPatient = idPatient;
		this.dateRendezVous = dateRendezVous;
		this.idRapport = idRapport;
	}
	
	public RendezVous(int idMedecin, int idPatient, int idRapport, Date dateRendezVous) {
		super();
		this.idMedecin = idMedecin;
		this.idPatient = idPatient;
		this.idRapport = idRapport;
		this.dateRendezVous = dateRendezVous;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdMedecin() {
		return idMedecin;
	}

	public void setIdMedecin(int idMedecin) {
		this.idMedecin = idMedecin;
	}

	public int getIdPatient() {
		return idPatient;
	}

	public void setIdPatient(int idPatient) {
		this.idPatient = idPatient;
	}
	
	public int getIdRapport() {
		return idRapport;
	}

	public void setIdRapport(int idRapport) {
		this.idRapport = idRapport;
	}

	public Date getDateRendezVous() {
		return dateRendezVous;
	}

	public void setDateRendezVous(Date dateRendezVous) {
		this.dateRendezVous = dateRendezVous;
	}
	
	
	
}
