package model;

import java.sql.Date;

import enums.Role;

public class Medecin extends Personne{
	
//	final private Role role = Role.MEDECIN;
	

	private String password;
	private String specialite;
	
	public Medecin() {
		super();
	}

	public Medecin(int id, String nom, String prenom, String sexe, String email, String password, String telephone,
			Date dateNaissance,String specialite) {
		super(id, nom, prenom, sexe, email, password, telephone, dateNaissance);
		this.specialite = specialite;
	}
	
	public Medecin(String nom, String prenom, String sexe, String email, String password, String telephone,
			Date dateNaissance,String specialite) {
		super(nom, prenom, sexe, email, telephone, dateNaissance);
		this.specialite = specialite;
		this.password = password;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}
/*
	public Role getRole() {
		return role;
	}
	
*/	
	
}
