package model;

public class Adress {
	
	private int id;
	private String rue;
	private String codePostal;
	private String ville;
	private int idPersonne;
	
	public Adress() {
	}
	
	public Adress(int id, String rue, String codePostal, String ville, int idPersonne) {
		this.id = id;
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.idPersonne = idPersonne;
	}
	public Adress(String rue, String codePostal, String ville, int idPersonne) {
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.idPersonne = idPersonne;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public int getIdPersonne() {
		return idPersonne;
	}

	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}
	
	
	
	
}
