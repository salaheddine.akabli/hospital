package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.PersonneDaoImpl;
import model.Personne;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		if(session.getAttribute("personne") != null){
			String role = "secretaire";
			if(role.equals("SECRETAIRE")) {
				this.getServletContext().getRequestDispatcher("/WEB-INF/secretaire/index.jsp").forward(req, resp);
			}else if(role.equals("MEDECIN")) {
				this.getServletContext().getRequestDispatcher("/WEB-INF/medecin/index.jsp").forward(req, resp);
			}else if(role.equals("MAJOR")) {
				this.getServletContext().getRequestDispatcher("/WEB-INF/major/index.jsp").forward(req, resp);
			}
		}else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getParameter("method").equals("connecter")) {
			if (!req.getParameter("email").isEmpty() && !req.getParameter("password").isEmpty()) {
				String email = req.getParameter("email");
				String password = req.getParameter("password");
				PersonneDaoImpl dao = new PersonneDaoImpl();
				if(dao.getByEmailAndPassword(email,password) != null) {
					HttpSession session = req.getSession();
					Personne personne = dao.getByEmailAndPassword(email,password);
					session.setAttribute("personne", personne);
					String role = "secretaire";
					if(role.equals("secretaire")) {
						this.getServletContext().getRequestDispatcher("/WEB-INF/secretaire/index.jsp").forward(req, resp);
					}else if(role.equals("medecin")) {
						this.getServletContext().getRequestDispatcher("/WEB-INF/medecin/index.jsp").forward(req, resp);
					}else if(role.equals("major")) {
						this.getServletContext().getRequestDispatcher("/WEB-INF/major/index.jsp").forward(req, resp);
					}else {
						this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
					}
				}else {
					boolean error = true;
					String errorMessage = "Ce compte n'existe pas !!";
					req.setAttribute("error", error);
					req.setAttribute("errorMessage", errorMessage);
					this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
				}
			}else {
				boolean error = true;
				String errorMessage = "Merci d'entrer vos cordonnées !!";
				req.setAttribute("error", error);
				req.setAttribute("errorMessage", errorMessage);
				this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
			}
		}else if(req.getParameter("method").equals("deconnecter")){
			HttpSession session = req.getSession();
			session.invalidate();
			this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
		}else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
		}
	}
}
