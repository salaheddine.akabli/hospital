<nav class="navbar navbar-expand-lg bg-body-tertiary text-bg-Light ">
		  <div class="container-fluid">
		    <a class="navbar-brand" href="#">Home</a>
		    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		      <span class="navbar-toggler-icon"></span>
		    </button>
		    <div class="collapse navbar-collapse" id="navbarSupportedContent">
		      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
		        <li class="nav-item">
		          <a class="nav-link active" aria-current="page" href="#">Home</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#">Link</a>
		        </li>
		      </ul>
		      <form class="d-flex" action="<%=request.getContextPath()%>/login" method="post">
		      	<input type="text" name="method" value="deconnecter" hidden="true">
		        <button class="btn btn-outline-success" type="submit">Se Deconnecter</button>
		      </form>
		    </div>
		 </div>
		 </nav>