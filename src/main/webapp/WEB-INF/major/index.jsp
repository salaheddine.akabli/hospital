<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	 <div class="container">
	 	<header>
			<jsp:include page="header.jsp" />
		</header>
		<aside>
			<jsp:include page="sidebar.jsp" />
		</aside>
		
		 <div class="container">
		 	<div class="col-md-6">
		 		<div class="card text-bg-info mt-3">
				  <div class="card-header">Patient</div>
				  <div class="card-body">
				    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				  </div>
				  <div class="card-footer text-bg-light">
				  	<a href="<%=request.getContextPath()%>/login">Consulter</a>
				  	<a href="<%=request.getContextPath()%>/login">Ajouter</a>
				  </div>
				</div>
		 	</div>
		 	<div class="col-md-6">
		 		<div class="card text-bg-info mt-3">
				  <div class="card-header">Medecin</div>
				  <div class="card-body">
				    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				  </div>
				  <div class="card-footer text-bg-light">
				  	<a href="<%=request.getContextPath()%>/login">Consulter</a>
				  	<a href="<%=request.getContextPath()%>/login">Ajouter</a>
				  	</div>
				</div>
		 	</div>
		 </div>
     </div>

</body>
</html>