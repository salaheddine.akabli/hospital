<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

</head>


<body>
	<div class="container">
		<header>
			<jsp:include page="header.jsp" />
		</header>
	</div>
    <aside>
		<jsp:include page="sidebar.jsp" />
	</aside>
	
	<div class="container">
	 	<div class="row">
	 		<div class="col-md-6">
	 		<div class="card text-bg-info mt-3">
			  <div class="card-header text-bg-light">Patient</div>
			  <div class="card-body">
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			  </div>
			  	<div class="card-footer text-bg-light row">
				  	<div class="col-6"><a href="<%=request.getContextPath()%>/secretaire/patient">Consulter</a></div>
				  	<div class="col-6 justify-center"><a href="<%=request.getContextPath()%>/addpatient">Ajouter</a></div>
				</div>
			</div>
	 	</div>
	 	<div class="col-md-6">
	 		<div class="card text-bg-info mt-3">
		  <div class="card-header text-bg-light">Medecin</div>
				  <div class="card-body">
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			  </div>
			  <div class="card-footer text-bg-light row">
			  	<div class="col-6"><a href="<%=request.getContextPath()%>/secretaire/patient">Consulter</a></div>
			  	<div class="col-6 justify-center"><a href="<%=request.getContextPath()%>/addpatient">Ajouter</a></div>
			  </div>
			</div>
	 	</div>
	 	</div>
	 </div>
</body>
</html>